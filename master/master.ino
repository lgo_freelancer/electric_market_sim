#include <Wire.h>
#include "_LiquidCrystal.h"
#include "xprintf.h"

#define NUMBER_OF_SLAVES 4
/* This array contains the I2C address for each slave device */
uint8_t slave_addresses[NUMBER_OF_SLAVES] = {0x01,0x02,0x03,0x04};

#define BUTTON_PIN 10

#define   CONTRAST_PIN   9
#define   CONTRAST       150
LiquidCrystal lcd(2, 3, 4, 5, 6, 7);

/* Comment out this line if you don't want to use Serial debug */
#define DEBUG_ENABLE

/* I2C Commands */
#define PRICE 0xa3
#define READ_POWER 0xa4
#define IS_BUYING 0xa5
#define READ_CURRENT 0xa6

void setup(void){
      Wire.begin(); /* Initialize I2C */
      Serial.begin(9600); /* Initialize Serial port for debugging */

      /* If you want (optional) you can use pin 9 and a cap to control de LCD's contrast */
      pinMode(CONTRAST_PIN, OUTPUT);
      analogWrite(CONTRAST_PIN, CONTRAST);

      /* Button Input */
      pinMode(BUTTON_PIN, INPUT_PULLUP);

      /* Start and clean LCD */
      lcd.begin(16,2);
      lcd.clear();
}

void loop(void){

  /* We will send the current price to all slaves
    once per second */
  uint8_t price;
  /* Check which price should we send */
  price = get_current_price();
  uint8_t i = 0;
  /* Transmit the price to each slave */
  for( i=0; i<(NUMBER_OF_SLAVES); i++){
    transmit_price_to_slave(price, slave_addresses[i]);
  }

  /* Now lets poll each slave to see if they are "buying",
    and if so how much power are they using. */
  uint8_t is_buying[NUMBER_OF_SLAVES];
  int16_t slave_current[NUMBER_OF_SLAVES];
  int32_t slave_power[NUMBER_OF_SLAVES];

  int32_t total_power = 0;
  for(i=0; i<(NUMBER_OF_SLAVES); i++){
    is_buying[i] = slave_is_buying(slave_addresses[i]);
    if(is_buying[i] == 1){
      /* The slave is buying - Read current and power */
      delay(10);
      slave_current[i] = read_current_consumption(slave_addresses[i]);
      delay(10);
      slave_power[i] = read_power_consumption(slave_addresses[i]);
      total_power += slave_power[i]; /* Add slave power to the total power */
    }else{
      /* Not buying - Set slave current and power to 0 */
      slave_current[i] = 0;
      slave_power[i] = 0;
    }
  }


  /* Update LCD contents */
  lcd_update_contents(is_buying,slave_current,slave_power,total_power);

  /* Serial Debug */
#ifdef DEBUG_ENABLE
  static uint32_t start_time = 0;
  if((millis() - start_time) > 1000){
    start_time = millis();
    for(i=0; i<NUMBER_OF_SLAVES; i++){
     Serial.print("Slave #"); Serial.print(i + 1);
      if(is_buying[i] == 1){
        Serial.print(" Current: ");
        Serial.print(slave_current[i]);
        Serial.print(" mA Power: ");
        Serial.print(slave_power[i]);
        Serial.println(" mW");
      }else{
        Serial.println(" Not Buying");
      }
    }
    Serial.print("Total Power: ");
    Serial.print(total_power); Serial.println(" mW");
    Serial.println();
  }
#endif

  /* Finally, wait 500 msec. */
  delay(500);
}


/* -------------------- Helper functions --------------------------*/

/* get_current_price(void):
   return the current price based on time and the state of
   switches.
   HERE YOU CAN IMPLEMENT ANY PRICE VARIATION YOU WANT
*/
uint8_t get_current_price(void){
/* For example; 10 cents for 5 seconds, 20 cents for 5, and repeat */
    static uint32_t start_time = 0;
    uint32_t elapsed_time;
    uint8_t price;
    uint8_t button_discount = 0;
    static uint8_t mode = 0;

    static uint8_t button_prev_state = HIGH;
    uint8_t button_curr_state = digitalRead(BUTTON_PIN);
    /* If now the button is LOW and before was HIGH it means
      that the button is pressed
    */
    if((button_curr_state == LOW) && (button_prev_state == HIGH)){
      mode = !mode;
    }
    button_prev_state = button_curr_state;

    /* if mode = 1 -> button_discount=5 else button_discount=0; */
    if(mode){
      button_discount = 5;
    }else{
      button_discount = 0;
    }

    /* Calculate the time since the price cycle started */
    /* This will generate that the price will be 10 for 5 secs
       (minus posible "discount"), and 20 for 5 secs, and the back to 10 */
    elapsed_time = millis() - start_time;
    if(elapsed_time <= 5000){
      price = 10 - button_discount;
    }else if(elapsed_time <= 10000){
      price = 20 - button_discount;
    }else{
      /* Start a new Cycle */
      start_time = millis();
      price = 10 - button_discount;
    }
    return price;
}


/* transmit_price_to_slave(uint8_t prc, uint8_t address):
   Transmit the price 'prc' to the slave with the address.
*/
void transmit_price_to_slave(uint8_t prc, uint8_t address){
  /* Send PRICE command */
  Wire.beginTransmission(address); /* Prepare transmission to address */
  /* We will send two byte: the command and the value */
  Wire.write(PRICE); /* Command */
  Wire.write(prc); /* Value */
  Wire.endTransmission(); /* Send and close I2C transmission */
}

/* slave_is_buying(uint8_t address):
   It's a function to check if a slave is buying or no.
   returns 0 if the slave is not buying.
   returns 1 if the slave is buying.
*/
uint8_t slave_is_buying(uint8_t address){
  /* Send IS_BUYING command */
  Wire.beginTransmission(address); /* Prepare transmission to address */
  /* We will send one byte: the command */
  Wire.write(IS_BUYING); /* Command */
  Wire.endTransmission(); /* Send and close I2C transmission */

  /* Now wait for reply */
  Wire.requestFrom(address, 1); /* Wait to receive 1 byte from the slave */
  uint8_t ret_val; /* return value */
  ret_val = Wire.read(); /* Read return Value from I2C */
  return ret_val;
}

/* read_power_consumption(uint8_t address):
   It's a function to get the power from the slave.
   returns the power in mW.
*/
int32_t read_power_consumption(uint8_t address){
  /* Send READ_POWER command */
  Wire.beginTransmission(address); /* Prepare transmission to address */
  /* We will send one byte: the command */
  Wire.write(READ_POWER); /* Command */
  Wire.endTransmission(); /* Send and close I2C transmission */

  /* Wait for reply - 4 bytes. 'power' is 32 bits long */
  Wire.requestFrom(address, 4);  /* Wait to receive 4 bytes from the slave */
  uint8_t i;
  int32_t receive_power = 0;
  /* Read 3 bytes and construct the int32_t value by shifting each byte */
  for(i=0; i<(4-1); i++){
      receive_power |= (0x000000ff & (int32_t)Wire.read());
      receive_power <<= 8;
  }
  /* Read the las byte and return */
  receive_power |= (0x000000ff & (int32_t)Wire.read());
  return receive_power;
}

/* read_current_consumption(uint8_t address):
   It's a function to get the current from the slave.
   returns the current in mA.
*/
int16_t read_current_consumption(uint8_t address){
  /* Send READ_CURRENT command */
  Wire.beginTransmission(address); /* Prepare transmission to address */
  /* We will send one byte: the command */
  Wire.write(READ_CURRENT); /* Command */
  Wire.endTransmission(); /* Send and close I2C transmission */

  /* Wait for reply */
  uint8_t ret_val_h;
  uint8_t ret_val_l;
  /* Wait for reply - 2 bytes. 'current' is 16 bits long */
  Wire.requestFrom(address, 2);
  ret_val_h = Wire.read();
  ret_val_l = Wire.read();
  /* Shift the high byte and 'or' it with low byte, then return */
  return ( ((0x00ff & ret_val_h) << 8) | (0x00ff & ret_val_l) );
}

void lcd_update_contents(uint8_t * is_buy,int16_t * currents,int32_t * powers, uint32_t total_power){
  xdev_out(print_char);
  static uint8_t screen_n = 0;
  static uint32_t start_time = 0;

  /* Increment each second the screen_n variable which
     indicates the screen number to display.
     There are 5 screens to display:
     One for each slave and one for totals */
  if( (millis() - start_time) > 1000){
    start_time = millis();
    screen_n++;
    screen_n %= 5; /* Limit the values from 0 to 4 */
  }

  lcd.home(); /* Go to begining of the LCD */
  if(screen_n != 4){
    /* Slave screen */
    xprintf("Slave #%d %7s",screen_n,is_buy[screen_n]?"BUY":"NO BUY"); /* Print First Line */
    lcd.setCursor(0, 1); /* Go to second line */
    /* Print Second Line */
    xprintf("%2d.%03dA  %2d.%03dW",currents[screen_n]/1000,currents[screen_n]%1000,powers[screen_n]/1000,powers[screen_n]%1000);
  }else{
    /* Totals screen */
    xprintf("Totals          ");
    lcd.setCursor(0, 1);
    uint16_t total_current = 0;
    uint8_t i;
    /* Sum all currents */
    for(i=0;i<NUMBER_OF_SLAVES;i++){
      total_current += currents[i];
    }
    /* Print total current and total power */
    xprintf("%2d.%03dA  %2d.%03dW",total_current/1000, total_current % 1000,total_power/1000, total_power % 1000);
  }
}

/* Function to print one char. to the LCD */
void print_char(unsigned char c)
{
    if(c == '^') c = 0xdf;
    lcd.print((char)c);
}
