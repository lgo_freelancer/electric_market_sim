#include <Wire.h>

/* ADJUST FOR EACH SLAVE - possible values 0x01-0x02-0x03-0x04 */
#define MY_I2C_ADDRESS 0x01
#define MY_PRICE_THRESHOLD 10

#define OUTPUT_PIN 13
#define CURRENT_SENSOR_INPUT_PIN A0

#define SYSTEM_VOLTAGE 24
/* Senson Sensitivity in mV per A. */
#define SENSOR_SENSITIVITY_MV_A 185   /* for 5 A */
//#define SENSOR_SENSITIVITY_MV_A 100 /* for 20 A */
//#define SENSOR_SENSITIVITY_MV_A 66  /* for 30 A */

/* I2C Commands */
#define NONE 0x00
#define PRICE 0xa3
#define READ_POWER 0xa4
#define IS_BUYING 0xa5
#define READ_CURRENT 0xa6


/* Globals */
enum{NOT_BUYING=0, BUYING};
int16_t current; /* Current in mA */
int32_t power; /* Power in mW */
uint16_t price; /* The current price */
uint8_t state; /* It indicates is the slave is buying or not */

void setup(void){
      Wire.begin(MY_I2C_ADDRESS); /* Start I2C with MY_I2C_ADDRESS (slave) */
      Serial.begin(9600); /* Start Serial port */
       /* Register the Function to execute when the slave receive a byte */
      Wire.onReceive(receive_handler);
       /* Register the Function to execute when the slave is requested for a byte */
      Wire.onRequest(request_handler);

      /* Set OUTPUT_PIN as OUTPUT */
      pinMode(OUTPUT_PIN, OUTPUT);
}

void loop(void){

  if(price <= MY_PRICE_THRESHOLD){
    /* Let's buy electricity */
    state = BUYING;
    activate_output();
    /* Read current in mA */
    current = read_current_sensor(); /* read the current from A0 */
    //Serial.print("Current mA: ");
    //Serial.println(current);
    /* Calculate power */
    power = SYSTEM_VOLTAGE * (uint32_t)current;
  }else{
    /* Too expensive */
    state = NOT_BUYING;
    deactivate_output();
    current = 0;
    power = 0;
  }

  /* Print each second, in the Serial port, the information of current and power */
  static uint32_t start_time = 0;
  if((millis() - start_time) > 1000){
    start_time = millis();
    Serial.print("Current: ");
    Serial.print(current);
    Serial.println(" mA");
    Serial.print("Power: ");
    Serial.print(power);
    Serial.println(" mW");
  }
}


/* -------------------- Helper functions --------------------------*/
uint8_t value_requested = NONE; /* This global store the last requested command */

void receive_handler(int num_bytes){
  //Serial.print("Received I2C command: ");
  uint8_t rx_val;
  /* Read the first byte */
  rx_val = Wire.read();
  /* Now switch according to the command received */
  switch (rx_val) {
    case PRICE:
      //Serial.print("Price: ");
      /* It'a PRICE command, then read the actual price */
      if(Wire.available()){
        rx_val = Wire.read();
        price = rx_val;
      }
      //Serial.println(price);
      /* No value is requested for this command */
      value_requested = NONE;
      break;
    case READ_POWER:
      //Serial.println("Read Power");
      /* READ_POWER command; now at the master request we will now what he wants */
      value_requested = READ_POWER;
      break;
    case READ_CURRENT:
      //Serial.println("Read Current");
      /* READ_CURRENT command; now at the master request we will now what he wants */
      value_requested = READ_CURRENT;
      break;
    case IS_BUYING:
      //Serial.print("Is Buying? ");
      //Serial.println(state);
      /* IS_BUYING command; now at the master request we will now what he wants */
      value_requested = IS_BUYING;
    break;
    default:
      /* We received something unexpected - we don't care */
      value_requested = NONE;
    break;
  }
}

/* This funcion gets executed when the master wants some bytes from us */
void request_handler(void){
    //Serial.print("Request ");
    uint8_t tx_buffer[4]; /* Buffer to store the bytes to send */
    switch (value_requested) {
      case READ_POWER:
        /* The command received was READ_POWER.
          Send the four bytes*/
        //Serial.println("READ_POWER");
        /* Send 4 bytes of power */
        tx_buffer[0] = ((uint8_t)((power & 0xff000000)>>24));
        tx_buffer[1] = ((uint8_t)((power & 0x00ff0000)>>16));
        tx_buffer[2] = ((uint8_t)((power & 0x0000ff00)>>8));
        tx_buffer[3] = ((uint8_t)((power & 0x000000ff)));
        Wire.write(tx_buffer,4); /* Send the bytes */
       break;
      case IS_BUYING:
        //Serial.print("IS_BUYING: ");
        /* The command received was IS_BUYING.
          Send 1 byte - the state */
        Wire.write(state);
        break;
      case READ_CURRENT:
      /* The command received was READ_CURRENT.
        Send two bytes*/
        tx_buffer[0] = (uint8_t)((current & 0xff00)>>8);
        tx_buffer[1] = (uint8_t)(current & 0x00ff);
        Wire.write(tx_buffer,2);
        break;
      case NONE:
       break;
      default:
       break;
    }
}



/* read_current_sensor(void):
   return the current value in mA.
*/
int16_t read_current_sensor(void){
  int16_t voltage_count = 0;
  int16_t current_mA;

  /* Averaging */
  uint8_t i;
  for(i=0;i<32;i++){
    voltage_count += analogRead(CURRENT_SENSOR_INPUT_PIN);
  }
  voltage_count = voltage_count / 32;

  voltage_count -= 512; /* Offset */
  if(voltage_count < 0){ /* We will ignore the sign, if the read is 'negative' then turn it 'positive' */
    voltage_count = -voltage_count;
  }

  /* Convert the raw ADC value to the actual mA */
  /* 5000 / 1024 * 1000 = 4882 */
  current_mA = voltage_count * ( 4882 / SENSOR_SENSITIVITY_MV_A);
  return current_mA;

  /*float average = 0;
  for(int i = 0; i < 1000; i++) {
    average = average + (.0264 * analogRead(A0) -13.51) / 1000;
    //5A mode, if 20A or 30A mode, need to modify this formula to
      //(.19 * analogRead(A0) -25) for 20A mode and
      //(.044 * analogRead(A0) -3.78) for 30A mode

    delay(1);
  }

  average = average / 1000.0;
   return (int16_t) average;*/

}

void activate_output(void){
  digitalWrite(OUTPUT_PIN,HIGH);
}

void deactivate_output(void){
  digitalWrite(OUTPUT_PIN,LOW);
}
